import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';

class AvatarWidget extends StatelessWidget {
  final Map<String, dynamic> profile;
  const AvatarWidget({Key? key, required this.profile}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: Alignment.bottomCenter,
      children: [
        Padding(
          padding: const EdgeInsets.only(
            bottom:12.0),
          child: GestureDetector(
            onTap: () => print("go to user profile") ,
            child: Container(
            padding: EdgeInsets.all(3.0),
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              border: Border.all(
                color: Colors.amber,
                width: 2,
              )
            ),
            child: CircleAvatar(
                radius: 25.0,
                backgroundImage: NetworkImage(
                  profile['avatar'],
                ),
              ),
            
          ),
          ),
        ),
        GestureDetector(
          onTap: () => print("Follow"),
          child: CircleAvatar(
            backgroundColor: Colors.pink,
            radius: 15.0,
            child: Icon(
              Icons.add,
              color: Colors.white,
              size: 20.0,
            ),
          ),
        )
      ],
    );
  }
}