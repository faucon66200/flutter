import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:intl/intl.dart';

class LikeWidget extends StatelessWidget {
  final int count;
  final Function() ? onPressed;
  const LikeWidget({Key? key, required this.count, this.onPressed}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.only(
            bottom:8.0),
          child: ClipOval(
            child: BackdropFilter(filter: ImageFilter.blur(
              sigmaX: 10,
              sigmaY: 10,
            ),
            child: Container(
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                color: Colors.white.withOpacity(0.1)
              ),
              child:  Center(
                child: IconButton(
                  onPressed: onPressed,
                  icon: Icon(Icons.favorite_border_rounded,
                  color: Colors.white,
                  size: 25,
                  ),
                  ),
                  
              ),
            ),
            ),
          ),
        ),
        Text(
          NumberFormat.compact().format(count),
          style: TextStyle(
            color: Colors.white,
            fontWeight: FontWeight.w500,
            letterSpacing: .8,
          ),
        )
      ],

    );
  }
}