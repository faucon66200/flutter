import 'package:flutter/material.dart';
import 'package:toktok/components/video_player/video_player_component.dart';
import 'package:toktok/components/post/post_component.dart';

const List<Map<String, dynamic>> data = [
  {
    "profile":{
      "username":"jhondoe",
      "avatar": "https://i.pravatar.cc/300?id=1"
    },
    "media":'https://assets.mixkit.co/videos/preview/mixkit-woman-running-above-the-camera-on-a-running-track-32807-large.mp4',
    "description":"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.",
    "likeCount": 13000,
    "commentCount":349,
},
{
  "profile":{
      "username":"jhonplu",
            "avatar": "https://i.pravatar.cc/300?id=2"

    },
    "media":'https://assets.mixkit.co/videos/preview/mixkit-young-photographer-setting-up-his-camera-outdoors-34408-large.mp4',
    "description":"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.",
    "likeCount": 1000,
    "commentCount":39,
},
{
  "profile":{
      "username":"jhontoz",
            "avatar": "https://i.pravatar.cc/300?id=3"

    },
    "media":'https://assets.mixkit.co/videos/preview/mixkit-waves-in-the-water-1164-large.mp4',
    "description":"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.",
    "likeCount": 1300000,
    "commentCount":1400,
} 
];
class HomeView extends StatelessWidget {
  const HomeView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBody: true,
      body: PageView(
        scrollDirection: Axis.vertical,
        children: data.map((item){
          return PostComponent(item : item);
        }).toList()
      ),
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: 0,
        backgroundColor: Colors.transparent,
        elevation: 0,
        unselectedItemColor: Colors.white,
        selectedItemColor: Colors.amber,
        showSelectedLabels: false,
        showUnselectedLabels: false,
        items: const[
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            label: "Home",
            ),
          BottomNavigationBarItem(
            icon: Icon(Icons.add_circle_outline_outlined),
            label: "Home",),
          BottomNavigationBarItem(
            icon: Icon(Icons.message_outlined),
            label: "Home",)
        ],
      ),
    );
  }
}