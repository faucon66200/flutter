import 'package:flutter/material.dart';
import 'package:toktok/components/video_player_progressbar/video_player_progressbar_component.dart';
import 'package:video_player/video_player.dart';
class VideoPlayerComponent extends StatefulWidget {
  final String media;
  
  const VideoPlayerComponent({Key? key, required this.media}) : super(key: key);

  @override
  State<VideoPlayerComponent> createState() => _VideoPlayerComponentState();
}

class _VideoPlayerComponentState extends State<VideoPlayerComponent> {
 late VideoPlayerController _controller;
double _position = 0.0;
 @override
  void initState() {

    super.initState();
    _controller = VideoPlayerController.network(
        widget.media,)
      ..initialize().then((_) {
        setState(() {});
       _controller.play();
       _controller.addListener(() { 
        setState(() {
          _position = double.parse((_controller.value.position.inMilliseconds / _controller.value.duration.inMilliseconds)
          .toStringAsFixed(2));
        });
       });
        _controller.setLooping(true);

      }
      
      );
      
  }
  @override
      void  dispose(){
        _controller.dispose();
        super.dispose();
      }
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        _controller.value.isInitialized
              ? SizedBox(
                width: _controller.value.size.width,
                height: _controller.value.size.height,
                child: VideoPlayer(_controller),
              )
              : Container(),
             Positioned(
              bottom: 90.0,
              left: 0.0,
              right: 0.0,
              child:  VideoPlayerProgressbarComponent(
                progress: _position,
                onTap: (position){
                    _controller.seekTo(Duration(milliseconds: (_controller.value.duration.inMilliseconds * position.toInt()))) ;    
                    setState(() => _position = position);
              },
            ),
          )
      ]);
  }
}