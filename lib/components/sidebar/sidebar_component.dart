import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:toktok/widgets/avatar_widget.dart';
import 'package:toktok/widgets/comment_widget.dart';
import 'package:toktok/widgets/like_widget.dart';

class SidebarComponent extends StatelessWidget {
  final Map<String , dynamic> item;
  const SidebarComponent({Key? key, required this.item}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Padding(
          padding: EdgeInsets.only(bottom: 20.0),
          child: AvatarWidget(
            profile: item['profile'],
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(bottom:20.0),
          child: LikeWidget(
            count :item['likeCount'],
            onPressed: () => print("Like"),
          ),
        ),
        CommentWidget(
          count: item['commentCount'],
          onPressed: () => print("Comment"),
        ),
      ],
    );
  }
}