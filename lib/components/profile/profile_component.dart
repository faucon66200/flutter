import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';

class ProfileComponent extends StatelessWidget {
  final Map<String, dynamic> item;
  const ProfileComponent({Key? key, required this.item}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children :[
             GestureDetector(
              onTap: () => print("Afficher le profil"),
              child:  Padding(
                padding: EdgeInsets.only(bottom:8.0),
                child :Text("@${item['profile']['username']}",style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                  letterSpacing: 1.0,
                  ),
                ),
              ),
             ),
            GestureDetector(
              onTap: () => print("Afficher plus"),
              child :  Text("@${item['description']}",
              style: TextStyle(
              color: Colors.white,
              letterSpacing: 1.0,
              height: 1.5,
            ),
            maxLines: 2,
            ),
            )
          ],
          );
  }
}