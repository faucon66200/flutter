import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:toktok/components/profile/profile_component.dart';
import 'package:toktok/components/sidebar/sidebar_component.dart';
import 'package:toktok/components/video_player/video_player_component.dart';
import 'package:toktok/widgets/avatar_widget.dart';

class PostComponent extends StatelessWidget {

  final Map<String,dynamic> item;
  const PostComponent({Key? key, required this.item}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        VideoPlayerComponent(
          media: item["media"]
          ),
         Positioned(
          bottom: 120.0,
          left: 20.0,
          right: 100.0,
          child: ProfileComponent(
            item: item,
          ),
       ),
       Positioned(
        bottom: 120.0,
        right : 20.0,
        child: SidebarComponent(item: item,),
        ),
      ],
    );
  }
}