import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';

class VideoPlayerProgressbarComponent extends StatelessWidget {
  final double progress;
  final Function(double position)? onTap; 
  const VideoPlayerProgressbarComponent({Key? key,this.progress = 0,this.onTap}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
    onTapUp: (TapUpDetails details){
      if(onTap != null){
        onTap!(double.parse((details.globalPosition.dx/ MediaQuery.of(context).size.width).toStringAsFixed(2))); 

      }
    },
    child: Padding(
        padding: const EdgeInsets.symmetric(
          vertical:4),
        child: Stack(
          children:[
             Container(
            height: 2,
            width: double.infinity,
            decoration: BoxDecoration(
              color: Colors.amberAccent.withOpacity(.2)
              ),
          ),
          Container(
          height: 2,
          width: MediaQuery.of(context).size.width * progress,
          decoration: BoxDecoration(
            color: Colors.amberAccent
            ),
        ),
          ],
        ),
        
      ),
    );
  }
}